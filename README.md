

## Assets

- [Superpowers Prehistoric Platformer Asset Pack](https://github.com/sparklinlabs/superpowers-asset-packs)
- ["Prehistoric Caveman" font by Darrell Flood](http://www.fontspace.com/darrell-flood/prehistoric-caveman)
- [Cinemachine](https://docs.unity3d.com/Packages/com.unity.cinemachine@2.1/manual/index.html)
- [2D Pixel Perfect](https://docs.unity3d.com/Packages/com.unity.2d.pixel-perfect@1.0/manual/index.html)
- [HSV Shader for Unity](https://github.com/greggman/hsva-unity)
