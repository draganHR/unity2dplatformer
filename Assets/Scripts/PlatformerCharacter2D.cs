using System;
using UnityEngine;

/**
 * Based on UnityStandardAssets._2D:
 *  - PlatformerCharacter2D
 *  - Platformer2DUserControl
 */

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class PlatformerCharacter2D : MonoBehaviour
{
    [SerializeField] private float maxSpeed = 10f;                  // The fastest the player can travel in the x axis.
    [SerializeField] private float jumpForce = 400f;                // Amount of force added when the player jumps.
    [SerializeField] private bool airControl = false;               // Whether or not a player can steer while jumping;
    [SerializeField] private LayerMask groundMask;                  // A mask determining what is ground to the character
    [SerializeField] private LayerMask slipperyMask;                // A mask determining slippery ground
    [SerializeField] private float slipperyMultiplier = 1f;

    private Transform groundCheck;       // A position marking where to check if the player is grounded.
    const float groundedRadius = 0.2f; // Radius of the overlap circle to determine if grounded
    private bool grounded;               // Whether or not the player is grounded.
    private bool slippery;
    const float ceilingRadius = .01f;    // Radius of the overlap circle to determine if the player can stand up
    private Animator animator;           // Reference to the player's animator component.
    private Rigidbody2D rb2d;
    private bool facingRight = true;   // For determining which way the player is currently facing.

    public delegate void EventHandler(PlatformerCharacter2D component);
    public event EventHandler OnJump;

    private void Awake()
    {
        // Setting up references.
        groundCheck = transform.Find("GroundCheck");
        animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }
    
    private void FixedUpdate()
    {
        grounded = false;
        slippery = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        // This can be done using layers instead but Sample Assets will not overwrite your project settings.
        Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, groundedRadius, groundMask);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                grounded = true;
                slippery = (slipperyMask == (slipperyMask | (1 << colliders[i].gameObject.layer)));
            }
        }
        animator.SetBool("grounded", grounded);

        // Set the vertical animation
        animator.SetFloat("velocityY", rb2d.velocity.y);

    }

    public void Move(float move, bool jump)
    {
        //only control the player if grounded or airControl is turned on
        if (grounded || airControl)
        {
            // The Speed animator parameter is set to the absolute value of the horizontal input.
            animator.SetFloat("velocityX", Mathf.Abs(move));

            float modifiedMove;

            // Move the character
            if (slippery)
            {
                modifiedMove = Mathf.Lerp(rb2d.velocity.x / maxSpeed, move, slipperyMultiplier * Time.deltaTime);
            }
            else
            {
                modifiedMove = move;
            }

            rb2d.velocity = new Vector2(modifiedMove * maxSpeed, rb2d.velocity.y);

            if ((move > 0 && !facingRight) || (move < 0 && facingRight))
            {
                Flip();
            }
        }
        // If the player should jump...
        if (grounded && jump)
        {
            // Add a vertical force to the player.
            grounded = false;
            animator.SetBool("grounded", false);
            rb2d.AddForce(new Vector2(0f, jumpForce));

            if (OnJump != null)
            {
                OnJump(this);
            }
        }
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
