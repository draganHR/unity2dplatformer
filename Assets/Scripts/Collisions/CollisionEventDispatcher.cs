﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*

Usage:

    private ChildCollider[] childColliders;

    void Awake()
    {
        childColliders = GetComponentsInChildren<CollisionEventDispatcher>();
    }

    public void OnEnable()
    {
        foreach (var childCollider in childColliders)
        {
            childCollider.CollisionEvent += ChildCollisionHandler;
        }
    }

    public void OnDisable()
    {
        foreach (var childCollider in childColliders)
        {
            childCollider.CollisionEvent -= ChildCollisionHandler;
        }
    }

    private void ChildCollisionHandler(GameObject child, GameObject collissionWithObject )
    {
        Debug.Log("Child Collider " + child.name + "  collided with " + collissionWithObject.name);
    }

*/

public class CollisionEventDispatcher : MonoBehaviour {

    public LayerMask canCollide;
    public LayerMask canTrigger;
    public delegate void CollisionEventHandler(GameObject child, GameObject collisionWithObject);
    public event CollisionEventHandler CollisionEnterEvent;

    public event CollisionEventHandler TriggerEnter2DEvent;
    public event CollisionEventHandler TriggerExit2DEvent;
    public event CollisionEventHandler TriggerStay2DEvent;

    void OnCollisionEnter(Collision collision)
    {
        int layer = 1 << collision.gameObject.layer;

        if ((layer & canCollide) == 0)
            return;

        if (CollisionEnterEvent != null)
            CollisionEnterEvent(gameObject, collision.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        int layer = 1 << collision.gameObject.layer;

        if ((layer & canTrigger) == 0)
            return;

        if (TriggerEnter2DEvent != null)
            TriggerEnter2DEvent(gameObject, collision.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        int layer = 1 << collision.gameObject.layer;

        if ((layer & canTrigger) == 0)
            return;

        if (TriggerExit2DEvent != null)
            TriggerExit2DEvent(gameObject, collision.gameObject);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        int layer = 1 << collision.gameObject.layer;

        if ((layer & canTrigger) == 0)
            return;

        if (TriggerStay2DEvent != null)
            TriggerStay2DEvent(gameObject, collision.gameObject);
    }

}
