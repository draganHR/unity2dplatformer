﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToCamera : MonoBehaviour {

    private CameraController cc;

    void Start () {
        cc = FindObjectOfType<CameraController>();
        transform.parent = cc.transform;
        transform.position = Vector3.zero;
    }
}
