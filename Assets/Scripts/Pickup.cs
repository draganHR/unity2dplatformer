﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {
    private bool pickedUp;
    public float waitBeforeDestory = 5.0f;
    public GameObject spawnOnPickup;

    // Use this for initialization
    void Start () {
        //pickedUp = false;
    }
	
    /*
    public void PickUp()
    {
        if (pickedUp)
        {
            return;
        }

        Debug.Log("Pick up " + this);
        pickedUp = true;

        GetComponent<AudioSource>().Play();

        Destroy(gameObject, waitBeforeDestory);
    }*/

    public void PickUp()
    {

        Debug.Log("Pick up " + this);
        Instantiate(spawnOnPickup, transform.position, Quaternion.identity);
        // GameController.instance.PlayerScored(value);
        Destroy(gameObject);

    }

}
