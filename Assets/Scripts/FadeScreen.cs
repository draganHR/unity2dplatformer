﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class FadeScreen : MonoBehaviour {
    public Animator animator;

    public delegate void FadeCallback();

    private FadeCallback fadeInCallback;
    private FadeCallback fadeOutCallback;

    void Start () {
        animator = GetComponent<Animator>();
    }
	
    public void FadeIn(FadeCallback callback = null)
    {
        fadeInCallback = callback;
        animator.SetTrigger("FadeIn");
    }

    public void FadeOut(FadeCallback callback = null)
    {
        fadeOutCallback = callback;
        animator.SetTrigger("FadeOut");
    }

    private void OnFadeOut()
    {
        if(fadeOutCallback != null)
        {
            fadeOutCallback();
        }
    }

    private void OnFadeIn()
    {
        if (fadeInCallback != null)
        {
            fadeInCallback();
        }
    }
}
