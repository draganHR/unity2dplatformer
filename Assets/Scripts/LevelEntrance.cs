﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEntrance : Interactable
{
    public string sceneName;

    public override void Interact(PlayerController player)
    {
        Debug.Log("LevelEntrance: Load scene " + sceneName);
        GameController.instance.FadeOutAndLoadScene(sceneName);
    }
}
