﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour {
    public string message;
    private float messageTimeout = 8f;

    // Use this for initialization
    void Start () {

    }
	
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.isTrigger && collision.gameObject.GetComponent<PlayerController>() != null)
        {
            GameController.instance.ShowMessage(message, messageTimeout);
            collision.gameObject.GetComponent<PlayerController>().setInteractable(this);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.isTrigger && collision.gameObject.GetComponent<PlayerController>() != null)
        {
            collision.gameObject.GetComponent<PlayerController>().removeInteractable();
        }
    }

    public virtual void Interact(PlayerController player)
    {
        Debug.Log("Interact " + player + " with " + this);
    }
}
