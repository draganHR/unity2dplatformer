﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class OneWayPlatform : MonoBehaviour {

    private Collider2D platformCollider;
    private Collider2D triggerCollider;

    private void Start()
    {
        gameObject.layer = LayerMask.NameToLayer("OneWayPlatform");

        Collider2D[] colliders = GetComponents<Collider2D>();
        foreach (Collider2D collider in colliders)
        {
            if(collider.isTrigger)
            {
                triggerCollider = collider;
            } else
            {
                platformCollider = collider;
            }
        }

        if (platformCollider == null)
        {
            Debug.LogError("platformCollider not found!");
        }
        if (triggerCollider == null)
        {
            Debug.LogError("triggerCollider not found!");
        }
        if (colliders.Length > 2)
        {
            Debug.LogError("Object contains more than 2 colliders!");
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(platformCollider.IsTouching(collision))
        {
            return;
        }
        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
            platformCollider.enabled = false;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
            platformCollider.enabled = true;
        }
    }

}
