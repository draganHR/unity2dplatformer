﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(BoxCollider2D))]
public class Checkpoint : MonoBehaviour {
    private ParticleSystem particles;
    private AudioSource audioSource;
    private bool isActive;

    void Awake() {
        particles = GetComponent<ParticleSystem>();
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isActive && collision.gameObject.GetComponent<PlayerController>() != null)
        {
            GameController.instance.activateCheckpoint(this);
            audioSource.Play();
        }
    }

    public void Activate()
    {
        isActive = true;
        particles.Play();
    }

    public void Deactivate()
    {
        isActive = false;
        particles.Stop();
    }
}
