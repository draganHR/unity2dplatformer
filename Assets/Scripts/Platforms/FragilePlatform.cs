﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(Animator))]
public class FragilePlatform : MonoBehaviour {

    private AudioSource audioSource;
    private Animator animator;

    public float destroyTimeout = 0.6f;
    public float restoreTimeout = 5.0f;
    public AudioClip beforeDestoySound;
    public AudioClip onDestroySound;
    private bool isDestoyed;

    void Start () {
        animator = GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
        audioSource.loop = false;
        audioSource.playOnAwake = false;
        audioSource.Stop();

        isDestoyed = false;
    }
	
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isDestoyed == false && collision.gameObject.GetComponent<PlayerController>() != null)
        {
            PlayAudioClip(beforeDestoySound);
            isDestoyed = true;
            animator.Play("Activated");
            StartCoroutine(DestroyPlatform());
        }
    }

    private IEnumerator DestroyPlatform()
    {
        yield return new WaitForSeconds(destroyTimeout);

        PlayAudioClip(onDestroySound);
        animator.SetTrigger("Destroy");

        StartCoroutine(RestorePlatform());
    }

    private IEnumerator RestorePlatform()
    {
        yield return new WaitForSeconds(restoreTimeout);

        animator.SetTrigger("Restore");
        isDestoyed = false;
    }

    private void PlayAudioClip(AudioClip clip)
    {
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
    }

}
