﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathManager))]
[RequireComponent(typeof(StickyObject))]
public class LiftPlatform : MonoBehaviour {
    public float speed = 0.1f;
    public float pauseOnStart = 0.5f;
    public float pauseOnMiddle = 0f;
    public float pauseOnReturn = 2.0f;

    private List<Vector3> nodes;
    private int targetNode;
    private int direction = 0;
    private bool paused;
    private int loaded;
    
    void Start () {
        nodes = GetComponent<PathManager>().nodes;
        targetNode = 0;
        transform.position = nodes[0];
        paused = false;
        loaded = 0;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (direction == 0)
        {
            direction = 1;
        }
        loaded += 1;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        loaded -= 1;
    }

    private void FixedUpdate()
    {
        if (!paused && direction != 0)
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.position, nodes[targetNode], speed);
            transform.position = newPosition;

            if (transform.position == nodes[targetNode])
            {

                float pauseSeconds;
                if (targetNode == 0)
                {
                    pauseSeconds = pauseOnStart;
                }
                if (targetNode == nodes.Count - 1)
                {
                    if(loaded > 0)
                    {
                        return;  // Don't go back until unloaded
                    }
                    pauseSeconds = pauseOnReturn;
                }
                else
                {
                    pauseSeconds = pauseOnMiddle;
                }

                StartCoroutine(Pause(pauseSeconds));

                NextNode();
            }
        }
    }

    private void NextNode()
    {
        if (direction == 1 && targetNode >= nodes.Count - 1)
        {
            targetNode = nodes.Count - 1;
            direction = -1;
        }
        else if (direction == -1 && targetNode <= 0)
        {
            targetNode = 0;
            direction = 0;
        }
        targetNode += direction;
    }

    private IEnumerator Pause(float seconds)
    {
        paused = true;
        yield return new WaitForSeconds(seconds);
        paused = false;
    }

}
