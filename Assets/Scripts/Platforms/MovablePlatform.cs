﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PathManager))]
[RequireComponent(typeof(StickyObject))]
public class MovablePlatform : MonoBehaviour
{
    public float speed = 0.1f;
    public float pauseOnEdge = 2.0f;
    public float pauseOnMiddle = 0f;
    public int initialNode = 0;

    private List<Vector3> nodes;
    private int targetNode;
    private int direction = 1;
    private bool paused;

    void Start () {
        nodes = GetComponent<PathManager>().nodes;
        targetNode = initialNode;
        transform.position = nodes[initialNode];
        paused = false;
    }
	
    private void FixedUpdate()
    {
        if(!paused)
        {
            Vector3 newPosition = Vector3.MoveTowards(transform.position, nodes[targetNode], speed);
            transform.position = newPosition;

            if (transform.position == nodes[targetNode]) {

                float pauseSeconds;
                if (targetNode == 0 || targetNode == nodes.Count - 1)
                {
                    pauseSeconds = pauseOnEdge;
                }
                else
                {
                    pauseSeconds = pauseOnMiddle;
                }

                StartCoroutine(Pause(pauseSeconds));

                NextNode();
            }
        }
    }

    private void NextNode()
    {
        if (targetNode >= nodes.Count - 1)
        {
            targetNode = nodes.Count - 1;
            direction = -1;
        }
        else if (targetNode <= 0) {
            targetNode = 0;
            direction = 1;
        }
        targetNode += direction;
    }

    private IEnumerator Pause(float seconds)
    {
        paused = true;
        yield return new WaitForSeconds(seconds);
        paused = false;
    }

}
