﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(Cinemachine.CinemachineBrain))]
public class CameraController : MonoBehaviour {

    private Transform target;
    private new Camera camera;
    private Cinemachine.CinemachineVirtualCamera cvc;

    void Start () {
        target = GameObject.Find("Player").transform;
        camera = GetComponent<Camera>();
        cvc = GetComponentInChildren<Cinemachine.CinemachineVirtualCamera>();
        cvc.Follow = target;

        PolygonCollider2D boundingShape = null;
        GameObject cameraBoundary = GameObject.Find("CameraBoundary");
        if(cameraBoundary != null)
        {
            boundingShape = cameraBoundary.GetComponent<PolygonCollider2D>();
        }

        DebugBoundingShape(boundingShape);
        cvc.GetComponent<Cinemachine.CinemachineConfiner>().m_BoundingShape2D = boundingShape;

        InstantMoveToTarget();
    }

    public void InstantMoveToTarget()
    {
        cvc.enabled = false;
        InstantMoveToPosition(target.position);
        cvc.enabled = true;
    }

    public void InstantMoveToPosition(Vector3 position)
    {
        Vector3 point = camera.WorldToViewportPoint(target.position);
        Vector3 delta = target.position - camera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, point.z));
        Vector3 destination = transform.position + delta;
        transform.position = destination;
    }

    public void ToggleCvc(bool enabled)
    {
        cvc.enabled = enabled;
    }

    /**
     * Checks PolygonCollider2D lines are straight to avoid camera glytches
     */
    public void DebugBoundingShape(PolygonCollider2D boundingShape)
    {
        if (boundingShape == null)
        {
            Debug.LogWarning("Camera boundary not found!");
            return;
        }

        int numberOfPoints = boundingShape.GetTotalPointCount();
        Vector2 pointA;
        Vector2 pointB;

        for (int a = 0; a < numberOfPoints; a++)
        {
            int b = a == numberOfPoints -1 ? 0 : a + 1;
            pointA = boundingShape.points[a];
            pointB = boundingShape.points[b];
            if(pointA.x != pointB.x && pointA.y != pointB.y)
            {
                Debug.LogWarning("Camera boundary line is not straight: " + pointA + " - " + pointB);
            }
        }

    }
}
