﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class FlyingMonster : NpcBase
{
    private Rigidbody2D rb2d;
    public float speed = 2f;

    public AudioClip attackSound;
    public AudioClip deathSound;

    protected override void Start()
    {
        base.Start();

        rb2d = GetComponent<Rigidbody2D>();
        rb2d.isKinematic = true;
        rb2d.collisionDetectionMode = CollisionDetectionMode2D.Discrete;
    }

    private void Update()
    {
        if(isAlive)
        {
            Vector3 step = Vector3.left * Time.deltaTime * speed;
            transform.position += step;
        }
    }

    protected override void OnDealDamage(PlayerController player, GameObject target)
    {
        if(isAlive)
        {
            player.Hurt();
            // animator.Play("Attack");
            PlayAudioClip(attackSound);
        }
    }

    protected override void OnStomped(PlayerController player, GameObject target)
    {
        if (isAlive)
        {
            player.OnStomp();
            isAlive = false;
            // animator.Play("Dead");
            PlayAudioClip(deathSound);
            rb2d.isKinematic = false;
            rb2d.velocity = new Vector2(0f, -3.0f);
        }
    }

}
