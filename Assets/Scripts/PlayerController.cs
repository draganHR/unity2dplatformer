﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CapsuleCollider2D))]
[RequireComponent(typeof(PlatformerCharacter2D))]
[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour {
    public AudioClip jumpSound;
    public AudioClip deathSound;
    private Animator animator;
    private AudioSource audioSource;
    private Rigidbody2D rb2d;

    private int killZoneLayer;
    private bool isAlive;

    private const float axisThreshold = 0.2f;

    private bool doJump;
    [SerializeField] private float stompSpeed = 14f;

    private Interactable interactable;
    PlatformerCharacter2D pc;

    private const string killZoneTag = "KillZone";

    // Use this for initialization
    void Awake()
    {
        pc = GetComponent<PlatformerCharacter2D>();

        rb2d = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        
        isAlive = true;
    }

    public void OnEnable()
    {
        pc.OnJump += OnJumpEvent;
    }

    public void OnDisable()
    {
        pc.OnJump -= OnJumpEvent;
    }

    private void Update()
    {
        if (isAlive && !doJump)
        {
            // Read the jump input in Update so button presses aren't missed.
            doJump = Input.GetButtonDown("Jump");
        }
    }

    private void FixedUpdate()
    {
        float horizontal = 0f;
        float vertical = 0f;
        if (isAlive)
        {
            // Read the inputs.
            horizontal = Input.GetAxis("Horizontal");
            if(horizontal < axisThreshold && horizontal > -axisThreshold)
            {
                horizontal = 0f;
            }

            vertical = Input.GetAxis("Vertical");
            if (vertical < axisThreshold && vertical > -axisThreshold)
            {
                vertical = 0f;
            }

            if (vertical != 0f)
            {
                if (interactable != null)
                {
                    interactable.Interact(this);
                    interactable = null;  // Prevent triggering interactable.Interact multiple times
                }
            }

        }

        // Pass all parameters to the character control script.
        pc.Move(horizontal, doJump);
        doJump = false;
    }

    public bool getIsAlive()
    {
        return isAlive;
    }

    public void Respawn(Vector3 position)
    {
        this.transform.position = position;
        rb2d.velocity = Vector3.zero;

        isAlive = true;
        animator.Play("Idle");

        FindObjectOfType<CameraController>().InstantMoveToTarget();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isAlive)
        {
            if (collision.gameObject.tag == killZoneTag)
            {
                Hurt();
                return;
            }

            Pickup pickup = collision.GetComponent<Pickup>();
            if (pickup != null)
            {
                pickup.PickUp();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (isAlive)
        {
            if (collision.gameObject.tag == killZoneTag)
            {
                Hurt();
                return;
            }
        }
    }

    public void Hurt()
    {
        if(isAlive)
        {
            isAlive = false;
            animator.Play("Dead");
            PlayAudioClip(deathSound);
            GameController.instance.OnDeath(this);
        }
    }

    public void OnStomp()
    {
        rb2d.velocity += new Vector2(rb2d.velocity.x, stompSpeed);
    }

    public void setInteractable(Interactable interactable)
    {
        this.interactable = interactable;
    }

    public void removeInteractable()
    {
        interactable = null;
    }

    private void PlayAudioClip(AudioClip clip) {
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
    }

    private void OnJumpEvent(PlatformerCharacter2D component)
    {
        PlayAudioClip(jumpSound);
    }

}
