﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PathManager))]
public class PathManagerEditor : Editor
{
    private int currentNode = 0;
    private bool moveAllNodes = false;

    private void OnDisable()
    {
        PathManager pathManager = ((PathManager)target);
        currentNode = 0;
        pathManager.transform.position = pathManager.nodes[currentNode];
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        PathManager pathManager = ((PathManager)target);

        GUILayout.BeginHorizontal("box");
        moveAllNodes = GUILayout.Toggle(moveAllNodes, new GUIContent("Lock nodes", "Move all nodes when Game Object is moved"));
        if (GUILayout.Button(new GUIContent("Reset", "Reset node positions to current Game Object position")))
        {
            ResetNodes();
        }
        GUILayout.EndHorizontal();
    }

    protected virtual void OnSceneGUI()
    {
        PathManager pathManager = ((PathManager)target);

        if (currentNode > 0 && currentNode > pathManager.nodes.Count - 1)
        {
            currentNode = 0;
        }

        float size = HandleUtility.GetHandleSize(pathManager.transform.position) * 0.1f;

        for (int i = 0; i < pathManager.nodes.Count; i++)
        {
            EditorGUI.BeginChangeCheck();
            Handles.color = Color.cyan;
            Vector3 newPosition = Handles.FreeMoveHandle(pathManager.nodes[i], Quaternion.identity, size, new Vector3(1.0f, 1.0f, 1.0f), Handles.RectangleHandleCap);

            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(pathManager, "Change node position");
                pathManager.nodes[i] = newPosition;

                if (currentNode != i)
                {
                    currentNode = i;
                }
                pathManager.transform.position = newPosition;
            }
        }
        MoveNodesWithObject();
    }

    private void MoveNodesWithObject()
    {
        PathManager pathManager = ((PathManager)target);

        Vector3 delta = pathManager.nodes[currentNode] - pathManager.transform.position;
        if (delta != Vector3.zero)
        {
            if (moveAllNodes)
            {
                for (int i = 0; i < pathManager.nodes.Count; i += 1)
                {
                    pathManager.nodes[i] -= delta;
                }
            }
            else
            {
                pathManager.nodes[currentNode] -= delta;
            }
        }
    }

    /**
     * Rests points to current platform position
     */
    public void ResetNodes()
    {
        PathManager pathManager = ((PathManager)target);

        if (pathManager.nodes.Count > 0)
        {
            for (int i = 0; i < pathManager.nodes.Count; i += 1)
            {
                pathManager.nodes[i] = pathManager.transform.position;
            }
        }
    }
}

#endif
