﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour {

    public List<Vector3> nodes;
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Vector3 previousNode = Vector3.positiveInfinity;
        foreach (Vector3 node in nodes)
        {
            Gizmos.DrawIcon(node, "Dot.png");
            if (previousNode != Vector3.positiveInfinity)
            {
                Gizmos.DrawLine(node, previousNode);
            }
            previousNode = node;
        }
    }
    
}
