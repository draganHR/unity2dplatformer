﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class DinoController : NpcBase {
    private Rigidbody2D rb2d;
    private SpriteRenderer spriteRenderer;

    public float speed = 80f;
    public Vector3 edgeDetectionAnchorOffset;
    public float edgeDetectionDistance = 0.32f;
    public float forwardDetectionDistance = 0.36f;

    public float idleTime = 1.5f;
    private bool isIdle;

    public LayerMask groundLayerMask;

    private Vector2 direction;

    public AudioClip attackSound;
    public AudioClip deathSound;

    private bool flipX = false;

    protected override void Start() {
        base.Start();

        rb2d = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        direction = Vector2.left;

        isIdle = false;
        animator.Play("Walk");
    }

    private void FixedUpdate()
    {
        if(!isIdle && isAlive)
        {
            Move();
        }
    }

    private void Move()
    {
        float rayDistance = 1.0f;
        Vector3 anchor = transform.position + edgeDetectionAnchorOffset;
        Vector3 otherPoint = anchor + new Vector3(direction.x * edgeDetectionDistance, 0f, 0f);
        RaycastHit2D hit1 = Physics2D.Raycast(anchor, Vector2.down, rayDistance, groundLayerMask);
        RaycastHit2D hit2 = Physics2D.Raycast(otherPoint, Vector2.down, rayDistance, groundLayerMask);
        
        RaycastHit2D hit_forward = Physics2D.Raycast(anchor, direction, forwardDetectionDistance, groundLayerMask);

        float angle = Mathf.Atan2(Mathf.Abs(hit1.distance - hit2.distance), edgeDetectionDistance) * Mathf.Rad2Deg;  // in degrees
        bool forwardHit = hit_forward.collider != null;

        Debug.DrawRay(anchor, Vector2.down * rayDistance, Color.blue);
        Debug.DrawRay(otherPoint, Vector2.down * rayDistance, Color.cyan);
        Debug.DrawRay(anchor, direction * forwardDetectionDistance, Color.green);

        // Debug.Log("angle: " + angle + "; hit1.distance: " + hit1.distance + "; hit2.distance: " + hit2.distance);

        // Switch direction when edge is detected
        if (angle > 5f || forwardHit) {
            direction = direction == Vector2.left ? Vector2.right : Vector2.left;  // Switch direction

            Idle();
        }

        if(!isIdle)
        {
            Vector3 velocity = direction * speed * Time.deltaTime;
            rb2d.velocity = velocity;

            // Flip SpriteRenderer when changing direction
            if (velocity.x > 0.01f)
            {
                if (flipX == false)
                {
                    Flip();
                }
            }
            else if (velocity.x < -0.01f)
            {
                if (flipX == true)
                {
                    Flip();
                }
            }

        }
    }

    private void Idle()
    {
        isIdle = true;
        animator.Play("Idle");
        rb2d.velocity = Vector3.zero;

        StartCoroutine(IdleWait());
    }

    private IEnumerator IdleWait()
    {
        yield return new WaitForSeconds(idleTime);

        if(isAlive)
        {
            isIdle = false;
            animator.Play("Walk");
        }
    }

    protected override void OnDealDamage(PlayerController player, GameObject target)
    {
        if (isAlive)
        {
            Idle();
            player.Hurt();
            animator.Play("Attack");
            PlayAudioClip(attackSound);
        }
    }

    protected override void OnStomped(PlayerController player, GameObject target)
    {
        if (isAlive)
        {
            player.OnStomp();
            isAlive = false;
            animator.Play("Dead");
            PlayAudioClip(deathSound);
        }
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        flipX = !flipX;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
