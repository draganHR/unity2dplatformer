﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnTrigger : MonoBehaviour {

    public GameObject prefab;
    public Vector2 initialVelocity;

    private GameObject instance;
    private bool done;

	void Awake () {
        done = false;
        GameController.getInstance().OnPlayerRespawn += OnPlayerRespawnEvent;
    }
	
    private void OnDestroy()
    {
        if(GameController.getInstance() != null)
        {
            GameController.getInstance().OnPlayerRespawn -= OnPlayerRespawnEvent;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(!done && collision.gameObject.GetComponent<PlayerController>() != null)
        {
            done = true;
            instance = Instantiate(prefab);
            instance.transform.position = transform.position;
            if(initialVelocity != Vector2.zero)
            {
                instance.GetComponent<Rigidbody2D>().velocity = initialVelocity;
            }
        }
    }

    private void OnPlayerRespawnEvent()
    {
        // Reset component state
        if(done)
        {
            Debug.Log("Reseting state: " + this);
            Destroy(instance);
            done = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 0.08f);
        Gizmos.DrawWireSphere(transform.position, 0.16f);
    }

}
