﻿using UnityEngine;
using UnityEditor;

public static class CameraUtils
{

    public static Bounds OrthographicBounds(this Camera camera)
    {
        Vector3 size = OrthographicSize(camera);
        Bounds bounds = new Bounds(camera.transform.position, size);
        return bounds;
    }

    public static Vector3 OrthographicSize(this Camera camera)
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = camera.orthographicSize * 2;
        return new Vector3(cameraHeight * screenAspect, cameraHeight, 0);
    }
}