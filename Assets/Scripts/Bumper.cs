﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(BoxCollider2D))]
public class Bumper : MonoBehaviour {

    protected Animator animator;
    protected AudioSource audioSource;

    public Vector2 velocity;
    public AudioClip bumperSound;

    void Start () {
        animator = GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
        audioSource.loop = false;
        audioSource.playOnAwake = false;
        audioSource.Stop();
    }

    // Update is called once per frame
    void Update () {

    }

    protected void PlayAudioClip(AudioClip clip)
    {
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Rigidbody2D>() != null)
        {
            animator.SetTrigger("activate");
            PlayAudioClip(bumperSound);
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = velocity;
        }
    }

}
