﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class GameController : MonoBehaviour {

    public static GameController instance;
    public List<AudioClip> audioTracks = new List<AudioClip>();
    private AudioSource audioSource;

    public bool paused;

    private Canvas canvas;
    private Text hintText;
    private int messageId = 0;

    private FadeScreen fadeScreen;
    private Image pauseMenu;

    private static System.Random random = new System.Random();

    private Checkpoint[] checkpoints;
    private Checkpoint activeCheckpoint;
    private PlayerController player;
    private CameraController camera;

    public delegate void EventHandler();
    public event EventHandler OnPlayerRespawn;

    public static GameController getInstance()
    {
        if(instance == null)
        {
            instance = GameObject.FindObjectOfType<GameController>();
        }
        return instance;
    }

    // Use this for initialization
    void Start()
    {
        GameController.instance = this;

        player = FindObjectOfType<PlayerController>();
        camera = FindObjectOfType<CameraController>();

        audioSource = GetComponent<AudioSource>();
        PlayRandomTrack();

        // Canvas mapping
        canvas = FindObjectOfType<Canvas>();
        fadeScreen = canvas.GetComponentInChildren<FadeScreen>();
        pauseMenu = canvas.transform.Find("PauseMenu").GetComponent<Image>();
        hintText = canvas.transform.Find("HintText").GetComponent<Text>();

        // Pause default state
        paused = false;
        pauseMenu.gameObject.SetActive(false);

        // Initialize checkpoints
        checkpoints = FindObjectsOfType<Checkpoint>();
        foreach (Checkpoint checkpoint in checkpoints) {
            if(activeCheckpoint == null || (activeCheckpoint.transform.position - player.transform.position).magnitude > (checkpoint.transform.position - player.transform.position).magnitude)
            {
                activeCheckpoint = checkpoint;
            }
        }
        toggleCheckpointStates();
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetButtonDown("Pause"))
        {
            TogglePause();
        }

        // Debug commands
        if (Input.GetKeyDown(KeyCode.F3))
        {
            JumpToNextCheckpoint();
        }
        else if (Input.GetKeyDown(KeyCode.F4))
        {
            FadeOutAndLoadScene("Level_Test");
        }
    }

    public void JumpToNextCheckpoint()
    {
        Debug.Log("Jump to next checkpoint");
        activeCheckpoint = GetNextCheckpoint();
        MovePlayerToActiveCheckpoint();
    }

    public Checkpoint GetNextCheckpoint()
    {
        Debug.Log("Jump to next checkpoint");
        bool foundNextCheckpoint = false;
        Checkpoint nextCheckpoint = checkpoints[0];
        foreach (Checkpoint checkpoint in checkpoints)
        {
            if (foundNextCheckpoint)
            {
                nextCheckpoint = checkpoint;
                break;
            }
            else if (checkpoint == activeCheckpoint)
            {
                foundNextCheckpoint = true;
            }
        }
        return nextCheckpoint;
    }

    public void TogglePause ()
    {
        Debug.Log("Toggle pause: " + (!paused));
        if (!paused)
        {
            pauseMenu.enabled = true;
            pauseMenu.gameObject.SetActive(true);
            Time.timeScale = 0;
            camera.ToggleCvc(false);
            paused = true;

            // Force menu background transparency
            pauseMenu.CrossFadeAlpha(0.0f, 0f, true);
            pauseMenu.CrossFadeAlpha(0.5f, 0.5f, true);
        }
        else
        {
            pauseMenu.enabled = false;
            pauseMenu.gameObject.SetActive(false);
            Time.timeScale = 1f;
            camera.ToggleCvc(true);
            paused = false;
        }
    }

    private void PlayRandomTrack() {
        int index = random.Next(audioTracks.Count);
        audioSource.clip = audioTracks[index];
        audioSource.Play();
    }
    
    public void ShowMessage(string text, float timeout=5f)
    {
        Debug.Log("ShowMessage: " + text);
        messageId += 1;
        hintText.text = text;
        StartCoroutine(WaitAndClearMessage(messageId, timeout));
    }

    IEnumerator WaitAndClearMessage(int clearMessageId, float timeout)
    {
        yield return new WaitForSeconds(timeout);

        Debug.Log("WaitAndClearMessage: " + (messageId == clearMessageId));
        if (messageId == clearMessageId)
        {
            hintText.text = "";
        }
    }

    public void OnDeath(PlayerController player)
    {
        StartCoroutine(WaitAndRespawn(player));
    }

    IEnumerator WaitAndRespawn(PlayerController player)
    {
        yield return new WaitForSeconds(3f);

        fadeScreen.FadeOut(() => {
            MovePlayerToActiveCheckpoint();
            if (OnPlayerRespawn != null)
            {
                OnPlayerRespawn();
            }
            fadeScreen.FadeIn();
        });

    }

    private void MovePlayerToActiveCheckpoint()
    {
        player.Respawn(activeCheckpoint.transform.position + new Vector3(0f, 0.32f, 0f));
    }

    public void activateCheckpoint(Checkpoint newCheckpoint)
    {
        Debug.Log("Activate checkpoint " + newCheckpoint);
        activeCheckpoint = newCheckpoint;
        toggleCheckpointStates();

        ShowMessage("Checkpoint!", 3f);
    }

    public void toggleCheckpointStates()
    {
        foreach (Checkpoint checkpoint in checkpoints)
        {
            if (activeCheckpoint == checkpoint)
            {
                checkpoint.Activate();
            }
            else
            {
                checkpoint.Deactivate();
            }
        }
    }

    public void FadeOutAndLoadScene(string sceneName)
    {
        fadeScreen.FadeOut(() => {
            player.Respawn(activeCheckpoint.transform.position + new Vector3(0f, 0.32f, 0f));
            SceneManager.LoadScene(sceneName);
        });
    }

}
