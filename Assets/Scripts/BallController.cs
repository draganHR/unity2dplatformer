﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class BallController : MonoBehaviour {
    private Rigidbody2D rb2d;
    public float killVelocity = 0.2f;

	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
    }
	
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
            Debug.Log("Collision with player: " + rb2d.velocity.magnitude);
            if (rb2d.velocity.magnitude > killVelocity)
            {
                collision.gameObject.GetComponent<PlayerController>().Hurt();
            }
        }
    }
}
