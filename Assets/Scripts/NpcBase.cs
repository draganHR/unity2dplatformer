﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
public class NpcBase : MonoBehaviour {

    protected Animator animator;
    protected AudioSource audioSource;

    private CollisionEventDispatcher[] childColliders;
    protected bool isAlive;

    protected virtual void Awake()
    {
        childColliders = GetComponentsInChildren<CollisionEventDispatcher>();
    }

    protected virtual void Start ()
    {

        animator = GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
        audioSource.loop = false;
        audioSource.playOnAwake = false;
        audioSource.Stop();

        isAlive = true;
    }

    public void OnEnable()
    {
        foreach (var childCollider in childColliders)
        {
            childCollider.TriggerEnter2DEvent += ChildTriggerEnter2DEvent;
        }
    }

    public void OnDisable()
    {
        foreach (var childCollider in childColliders)
        {
            childCollider.TriggerEnter2DEvent -= ChildTriggerEnter2DEvent;
        }
    }

    protected virtual void OnDealDamage(PlayerController player, GameObject target)
    {

    }

    protected virtual void OnStomped(PlayerController player, GameObject target)
    {

    }

    private void ChildTriggerEnter2DEvent(GameObject child, GameObject collissionWithObject)
    {
        Debug.Log("ChildTriggerEnter2DEvent " + child.name + "; isAlive " + isAlive);
        if (!isAlive)
        {
            return;
        }

        if (collissionWithObject.name == "Player")
        {
            PlayerController player = collissionWithObject.GetComponent<PlayerController>();
            if (!player.getIsAlive())
            {
                return;  // Already dead
            }

            if (child.name == "DamageCheck")
            {
                OnDealDamage(player, child);
                return;
            }
            else if (child.name == "StompCheck")
            {
                OnStomped(player, child);
                return;
            }
        }
    }

    protected void PlayAudioClip(AudioClip clip)
    {
        audioSource.Stop();
        audioSource.clip = clip;
        audioSource.Play();
    }

}
