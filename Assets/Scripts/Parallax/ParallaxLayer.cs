﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParallaxLayer : MonoBehaviour
{
    new private Camera camera;
    public float scrollFactorX = 0.25f;
    public float scrollFactorY = 1.0f;

    private float layerWidth;

    void Start()
    {
        camera = Camera.main;

        layerWidth = 0f;
        foreach (Transform child in transform)
        {
            layerWidth += child.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
        }
    }

    public void Move(Vector3 delta)
    {
        Vector3 newPos = transform.localPosition;
        newPos.x -= delta.x * scrollFactorX;
        newPos.y -= delta.y * scrollFactorY;
        transform.localPosition = newPos;
        ShiftSlices(delta);
    }

    private void ShiftSlices(Vector3 delta)
    {
        Bounds bounds = CameraUtils.OrthographicBounds(camera);

        float viewPortX = bounds.size.x / 2f;
        float cameraXLeft = bounds.center.x - viewPortX;
        float cameraXRight = bounds.center.x + viewPortX;

        foreach (Transform child in transform)
        {
            float childWidth = child.GetComponent<SpriteRenderer>().sprite.bounds.size.x;

            if (delta.x < 0f && child.position.x + childWidth < cameraXLeft)
            {
                // Shift layer slice right
                Vector3 position = child.transform.position;
                position.x = position.x + layerWidth;
                child.transform.position = position;
            }
            else if (delta.x > 0f && child.position.x - childWidth > cameraXRight)
            {
                // Shift layer slice left
                Vector3 position = child.transform.position;
                position.x = position.x - layerWidth;
                child.transform.position = position;
            }
        }
    }
}
