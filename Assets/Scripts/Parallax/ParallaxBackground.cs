﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ParallaxBackground : MonoBehaviour
{
    new private Camera camera;
    private List<ParallaxLayer> layers = new List<ParallaxLayer>();

    private Vector3 targetPosition;

    void Start()
    {
        camera = Camera.main;
        targetPosition = camera.transform.position;

        SetupLayers();
    }

    void SetupLayers()
    {
        layers.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            ParallaxLayer layer = transform.GetChild(i).GetComponent<ParallaxLayer>();

            if (layer != null)
            {
                layer.name = "Layer-" + i;
                layers.Add(layer);
            }
        }
    }

    void Update()
    {
        if (camera.transform.position != targetPosition)
        {
            Vector3 delta = targetPosition - camera.transform.position;
            Move(delta);
            targetPosition = camera.transform.position;
        }
    }
 
    void Move(Vector3 delta)
    {
        foreach (ParallaxLayer layer in layers)
        {
            layer.Move(delta);
        }
    }

    [System.Serializable]
    public class LayerProperties
    {
        public GameObject gameObject;
        public float scrollFactorX = 1f;
    }
}

