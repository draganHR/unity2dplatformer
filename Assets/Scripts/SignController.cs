﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SignController : MonoBehaviour {
    public string message;
    public float messageTimeout = 8f;
    private AudioSource audioSource;

	void Start () {
        audioSource = GetComponent<AudioSource>();
    }
	
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>() != null)
        {
            GameController.instance.ShowMessage(message, messageTimeout);
            audioSource.Play();
        }
    }
}
